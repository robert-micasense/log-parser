options=(GpsCsvReport VoltageReport Dls2ImuReport CameraImuReport AttitudesReport RecordCount Dls2SpectralReport FlightInfoCsvReport IrTemperatureReport Dls2DirectionalReport SatelliteReport UbloxCsvReport GyroBiasReport MinDelayReport TemperaturesReport IlsCsvReport GpsStatusReport CaptureTimeReport)

for o in ${options[@]}; do
	echo $o.csv
    paramlog_parser -o paramlog_$o.csv -r $o paramlog.dat
done
echo "done"