options=(SdReport CaptureTimeReport AgcReport)


micasense_log_parser.rb -d -o diag.log diag.dat

for o in ${options[@]}; do
	echo building $o.csv
    micasense_log_parser.rb -d -o $o.csv -r $o diag.dat
done
echo "done"