import re
import json
import sys


auto_cap_mode = ["disabled", "timer", "overlap", "external trigger"]
streaming_mode_sig = "trig=254"
promise_type = ["physical", "HTTP", "timer", "external", "overlap"]
configurations = []
streaming_on = False
version = "unknown"
latest_version = {"rededge": "5.1.12", "altum" :"1.3.6"}
camera = "rededge"

try:
	log_file = sys.argv[1]
except:
	log_file = "diag.log"

def main():
	global streaming_on, version, camera

	file = open(log_file, encoding="utf8", errors='ignore')
	for line in file:
		data = re.split("\[(.+?)\]", line)
		if "AppConfig" in data[3]:
			config = json.loads(data[4].replace("=>", ":"))
			configurations.append(config)
			if "AL" in config["camera_serial_number"]:
				camera = "altum"

		if "ImageSetWriter" in data[3]:
			if "SW Version: v" in data[4]: 
				version = (data[4].split(",")[0].split(": v")[1])

		if streaming_mode_sig in data:
			streaming_on = True

	for config in configurations:
		summary(config)

def qrMode(data):
	if ("QR" in data[4]):
		print(data[4].rstrip())
	if ("WARN" in data[1]):
		print (data[4].rstrip()) 
	if ("QR " in data[4].rstrip() or "QR-" in data[4].rstrip()):
		print(data[4].rstrip())

def summary(config):
	print("SUMMARY------------------------------")
	print("Serial:", config["camera_serial_number"])
	print("Version:", version)
	if version != latest_version[camera]:
		print("UPGRADE FIRMWARE. Latest is", latest_version[camera])
	else:
		print("FIRMWARE OK")
	print("Trigger:", auto_cap_mode[config["auto_cap_mode"]])
	if "timer" in auto_cap_mode[config["auto_cap_mode"]]:
		print("Timer interval:", config["timer_period"], "s")
	print("Altitude: ", config["operating_alt"], "m")
	print("Overlap", config["overlap_along_track"], "%")
	print("Streaming mode on?", streaming_on)



main()